<?php
/*
Template Name: Home-SliderOnly
*/

get_header ();
?>



<!-- ******************************************************************** This is the slider ********************************************************************-->

<!-- Start of slider wrapper -->
<section class="slider_wrapper">

<!-- Start of slider -->
<section class="slider">   

<ul class="slides">

	<?php
    $temp = $my_query;
    $my_query = null;
    $my_query = new WP_Query('post_type=slider&showposts=10');
    $my_query->query('post_type=slider&showposts=10');
    ?>
        
    <?php while ( $my_query->have_posts() ) : $my_query->the_post(); sltws_post_meta(); ?>
        
        <li>
        <a href="<?php echo $meta[ 'subtitle' ]; ?>" target="_new" title="<?php the_title(); ?>"><?php the_post_thumbnail('slide'); ?></a>
        </li>
        
        <?php endwhile; ?>    <?php wp_reset_query(); ?>
        
	</ul>
    


</section><!-- End of slider -->

<!-- Start of clear fix --><div class="clear"></div>

</section><!-- End of slider wrapper -->

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<!-- Start of main -->
<section id="main">

<?php the_content('        '); ?> 

<?php endwhile; endif; ?>

<!-- Start of clear fix --><div class="clear"></div>
            
</section><!-- End of main -->

<?php get_footer(); ?>