<?php
/*
Template Name: Photos
*/
?>

<?php get_header(); ?>

<!-- Start of main -->
<section id="main">

<!-- Start of main fullwidth wrapper -->
<div id="main_fullwidth_wrapper">

<!-- Start of portgallery wrapper -->
<article class="portgallery_wrapper">

<ul id="da-thumbs" class="da-thumbs">
<?php

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

query_posts($query_string.'&posts_per_page=6000000000&paged='.$paged. ''); if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
$portgallerylink = get_post_meta($post->ID, 'portgallerylink', $single = true);   
?>

<li class="box">
<a href="<?php echo ($portgallerylink); ?>">
<?php the_post_thumbnail('large'); ?>
<div><span><?php the_title (); ?></span></div>
</a>
</li>
<?php endwhile; ?> 
<?php else: ?> 
<p>There are no posts to display. Try using the search.</p> 
<?php endif; ?> 
</ul>

</article><!-- End of portgallery wrapper -->

<div class="clear"></div>

<div class="big"></div>

<hr />

</div><!-- End of main fullwidth wrapper -->

<!-- Start of clear fix --><div class="clear"></div>
            
</section><!-- End of main -->

<?php get_footer (); ?>