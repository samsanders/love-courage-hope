<?php
/*
Single Post Template: [Blog Single Right Sidebar]
Description: This part is optional, but helpful for describing the Post Template
*/
?>

<?php get_header(); ?>

<!-- Start of main -->
<section id="main">

<!-- Start of message center left -->
<div class="message_center_left">
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

<!-- Start of blog wrapper -->
<article class="blog_wrapper">
<?php 
if ( has_post_thumbnail() ) {  ?>
<!-- Start featured image -->
<div class="featured_image">

<a href="<?php the_permalink (); ?>"><?php the_post_thumbnail('slide'); ?></a>

</div><!-- End of featured image -->

<!-- Start of posted details -->
<div class="posted_details">

<div class="authorpic"></div>

<!-- Start of post content first -->
<div class="post_content_first">
<?php the_author() ?>

</div><!-- End of post content first -->

<div class="datepic"></div>

<!-- Start of post content -->
<div class="post_content">
<?php the_time('F jS, Y') ?>

</div><!-- End of post content -->

<?php
if (has_tag()) { ?>

<div class="tagpic"></div>

<!-- Start of post content -->
<div class="post_content">
<?php the_tags(__(''), ', '); ?>

</div><!-- End of post content -->

<?php } else { }?>

<!-- Start of post content last -->
<div class="post_content_last">
<?php if ('open' == $post->comment_status) { ?>
<?php comments_popup_link('0', '1', '%', 'comments-link'); ?>
<?php } ?>

</div><!-- End of post content last -->

</div><!-- End of posted details -->

<br />

<?php } else { ?>

<?php 
    if (has_post_format('gallery')) { ?>

<!-- Start featured image -->
<div class="featured_image_gallery">

<?php
$attachments = get_children(
array(
'post_type' => 'attachment',
'post_mime_type' => 'image',
'post_parent' => $post->ID
));
if(count($attachments) > 1) { ?>

<!-- Start of slider -->
<section class="slider">   

<ul class="slides">
<?php 

$args = array(
'post_type' => 'attachment',
'numberposts' => -1,
'post_status' => null,
'post_parent' => $post->ID
);

$attachments = get_posts( $args );
if ( $attachments ) {
foreach ( $attachments as $attachment ) {
echo '<li>';
echo wp_get_attachment_image( $attachment->ID, 'slide' );
echo '</li>';
}
}

?>

</ul><!-- End of slides -->	

</section><!-- End of slider -->

</div><!-- End of featured image -->

<?php  ;} ?>


<!-- Start of posted details -->
<div class="posted_details">

<div class="authorpic"></div>

<!-- Start of post content first -->
<div class="post_content_first">
<?php the_author() ?>

</div><!-- End of post content first -->

<div class="datepic"></div>

<!-- Start of post content -->
<div class="post_content">
<?php the_time('F jS, Y') ?>

</div><!-- End of post content -->

<?php
if (has_tag()) { ?>

<div class="tagpic"></div>

<!-- Start of post content -->
<div class="post_content">
<?php the_tags(__(''), ', '); ?>

</div><!-- End of post content -->

<?php } else { }?>

<!-- Start of post content last -->
<div class="post_content_last">
<?php if ('open' == $post->comment_status) { ?>
<?php comments_popup_link('0', '1', '%', 'comments-link'); ?>
<?php } ?>

</div><!-- End of post content last -->

</div><!-- End of posted details -->

<br />

<?php } else { ?>

<!-- Start of line break --><div class="hrr"></div>

<!-- Start of posted details -->
<div class="posted_details">

<div class="authorpic"></div>

<!-- Start of post content first -->
<div class="post_content_first">
<?php the_author() ?>

</div><!-- End of post content first -->

<div class="datepic"></div>

<!-- Start of post content -->
<div class="post_content">
<?php the_time('F jS, Y') ?>

</div><!-- End of post content -->

<?php
if (has_tag()) { ?>

<div class="tagpic"></div>

<!-- Start of post content -->
<div class="post_content">
<?php the_tags(__(''), ', '); ?>

</div><!-- End of post content -->

<?php } else { }?>

<?php if ('open' == $post->comment_status) { ?>
<!-- Start of post content last -->
<div class="post_content_last">

<?php comments_popup_link('0', '1', '%', 'comments-link'); ?>

</div><!-- End of post content last -->

<?php } ?>

</div><!-- End of posted details -->

<br />

<?php } } ?>

<!-- Start of line break --><div class="hrr"></div>   

<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

<!-- Start of featured text blog -->
<div class="featured_text_full">
<?php the_content('        '); ?> 

<?php endwhile; ?> 

<?php else: ?> 
<p><?php _e( 'There are no posts to display. Try using the search.', 'nature' ); ?></p> 

<?php endif; ?>

<!-- Start of social share wrapper -->
<div class="social_share_wrapper2">

<div class="socialpic"></div>

<!-- Start of social share links -->
<div class="social_share_links">
<a class="socialsharing" target="_blank" href="http://www.facebook.com/share.php?u=<?php the_permalink (); ?>"><?php _e( 'facebook', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="https://plus.google.com/share?url=<?php the_permalink (); ?>"><?php _e( 'google', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="http://twitter.com/home?status=<?php the_permalink (); ?>"><?php _e( 'twitter', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php the_permalink (); ?>"><?php _e( 'pinterest', 'nature' ); ?></a>
        
</div><!-- End of social share links -->

</div><!-- End of social share wrapper -->

<div class="clear"></div>

<div style="height:60px;"></div>

<hr />

<!-- Start of navigation -->
<div class="navigation">

<!-- Start of alignleft -->
<div class="alignleft">

<?php next_post('%', '', 'yes'); ?>

</div><!-- End of alignleft -->

<!-- Start of alignright -->
<div class="alignright">

<?php previous_post('%', '', 'yes'); ?> 

</div><!-- End of alignright -->

<!-- Start of clear fix --><div class="clear"></div>

</div><!-- End of navigation -->   

<?php if ('open' == $post->comment_status) { ?>
<?php comments_template(); ?>
<?php } ?>

</div><!-- End of featured text blog -->

<!-- Start of clear fix --><div class="clear"></div>      

</article><!-- End of blog wrapper -->

</div><!-- End of message center left -->

<!-- Start of blog right light -->
<div class="blog_right_light">
<?php get_sidebar ('page'); ?>            

</div><!-- End of blog right light -->

<!-- Start of clear fix --><div class="clear"></div>
            
</section><!-- End of main -->

<?php get_footer (); ?>