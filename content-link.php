<?php
/*
 * The default template for displaying link
 */
?>

<!-- Start of blog wrapper -->
<article class="blog_wrapper">

<?php // Get the text & url from the first link in the content
$content = get_the_content();
$link_string = extract_from_string('<a href=', '/a>', $content);
$link_bits = explode('"', $link_string);
foreach( $link_bits as $bit ) {
	if( substr($bit, 0, 1) == '>') $link_text = substr($bit, 1, strlen($bit)-2);
	if( substr($bit, 0, 4) == 'http') $link_url = $bit;
}?>

<h1 style="float:left;"><a href="<?php echo $link_url;?>"><?php the_title(); ?></a></h1>

<!-- Start of post content last -->
<div class="post_content_last" style="margin-top:30px;">
Link

</div><!-- End of post content last -->

<!-- Start of clear fix --><div class="clear"></div>

<br />

<!-- Start of posted details -->
<div class="posted_details">

<div class="datepic"></div>

<!-- Start of post content -->
<div class="post_content">
<?php the_time('F jS, Y') ?>

</div><!-- End of post content -->

<!-- Start of social share wrapper -->
<div class="social_share_wrapper">

<div class="socialpic"></div>

<!-- Start of social share links -->
<div class="social_share_links">
<a class="socialsharing" target="_blank" href="http://www.facebook.com/share.php?u=<?php the_permalink (); ?>"><?php _e( 'facebook', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="https://plus.google.com/share?url=<?php the_permalink (); ?>"><?php _e( 'google', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="http://twitter.com/home?status=<?php the_permalink (); ?>"><?php _e( 'twitter', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php the_permalink (); ?>"><?php _e( 'pinterest', 'nature' ); ?></a>
        
</div><!-- End of social share links -->

</div><!-- End of social share wrapper -->

</div><!-- End of posted details -->

<!-- Start of clear fix --><div class="clear"></div>

</article><!-- End of blog wrapper -->
