<?php
/*
Template Name: Left-Sidebar
*/
?>

<?php get_header(); ?>

<!-- Start of main -->
<section id="main">

<!-- Start of content right page -->
<div class="content_right_page">
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

<!-- Start of blog wrapper -->
<article class="blog_wrapper">

<!-- Start of featured text full -->
<div class="featured_text_full">

<?php the_content('        '); ?> 

<?php endwhile; ?> 

<?php else: ?> 
<p><?php _e( 'There are no posts to display. Try using the search.', 'nature' ); ?></p> 

<?php endif; ?>

</div><!-- End of featured text full -->

<!-- Start of clear fix --><div class="clear"></div>

</article><!-- End of blog wrapper -->

</div><!-- End of content right page -->

<!-- Start of blog left light -->
<div class="blog_left_light">
<?php get_sidebar ('page'); ?>            

</div><!-- End of blog left light -->

<!-- Start of clear fix --><div class="clear"></div>
            
</section><!-- End of main -->

<?php get_footer (); ?>