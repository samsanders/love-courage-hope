<?php
/*
Template Name: Contact
*/

get_header ();
?>

<!-- Start of main -->
<section id="main">

<!-- Start of clear fix --><div class="clear"></div>


<!-- Start of map div -->
<div id="map_div">

<!-- ****************************THIS IS THE START OF THE CONTACT MAP WIDGET & DYNAMIC CONTACT DETAILS FROM ADMIN**************************** -->

<!-- Start of one half first -->
<div class="one_half_first">

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$contactpagetitle = get_option_tree( 'vn_contactpagetitle' );
$companyaddress = get_option_tree( 'vn_contactaddress' );
$companycity = get_option_tree( 'vn_contactcity' );
$companyfax = get_option_tree( 'vn_contactfax' );
$companyphone = get_option_tree( 'vn_contactphone' );
$companyemailaddress = get_option_tree( 'vn_contactemail' );
} ?>

<?php if ($contactpagetitle != ('')){ ?> 
<h1><?php echo stripslashes($contactpagetitle); ?></h1>
<?php } else { } ?>

<!-- Start of textwidget -->
<div class="textwidget">

<!-- ****************************THIS IS THE START OF THE CONTACT INFORMATION**************************** -->

<?php if ($companyaddress != ('')){ ?> 

<ul class="contact">

<li>
<div class="mappic"></div>

<!-- Start of address div -->
<div class="addressdiv">
<?php echo stripslashes($companyaddress); ?> <br />
<?php echo stripslashes($companycity); ?>

</div><!-- End of address div -->

</li>

<?php } else { } ?>

<?php if ($companyphone != ('')){ ?> 

<li>
<div class="phonepic"></div>

<!-- Start of phone div -->
<div class="phonediv">
<?php echo stripslashes($companyphone); ?> 

</div><!-- End of phone div -->

</li>

<?php } else { } ?>

<?php if ($companyfax != ('')){ ?> 

<li>
<div class="faxpic"></div>

<!-- Start of fax div -->
<div class="faxdiv">
<?php echo stripslashes($companyfax); ?> 

</div><!-- End of fax div -->

</li>

<?php } else { } ?>

<?php if ($companyemailaddress != ('')){ ?> 

<li>
<div class="emailpic"></div>

<!-- Start of email div -->
<div class="emaildiv">
<a href="mailto:<?php echo ($companyemailaddress); ?>"><?php echo stripslashes($companyemailaddress); ?></a>

</div><!-- End of email div -->

</li>

<?php } else { } ?>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$facebook = get_option_tree( 'vn_facebook' );
} ?>

<?php if (isset($facebook)) { ?>

<li>
<div class="followpic"></div>

<!-- Start of follow div -->
<div class="followdiv">

<ul class="icons">

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$pinterest = get_option_tree( 'vn_pinterest' );
} ?>

<?php if (isset($pinterest)) { ?>

<li><a href="<?php echo $pinterest; ?>"><img src="<?php bloginfo('template_directory'); ?>/img/pinterest.png" height="18" width="13" alt="pinterest" /></a></li>

<?php } ?>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$flickrlink = get_option_tree( 'vn_flickr' );
} ?>

<?php if (isset($flickrlink)) { ?>

<li><a href="<?php echo $flickrlink; ?>"><img src="<?php bloginfo('template_directory'); ?>/img/flickr.png" height="18" width="20" alt="flickr" /></a></li>

<?php } ?>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$googlelink = get_option_tree( 'vn_googlelink' );
} ?>

<?php if (isset($googlelink)) { ?>

<li><a href="<?php echo $googlelink; ?>"><img src="<?php bloginfo('template_directory'); ?>/img/googleplus.png" height="15" width="16" alt="google plus" /></a></li>

<?php } ?>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$twitter = get_option_tree( 'vn_twitter' );
} ?>

<?php if (isset($twitter)) { ?>

<li><a href="<?php echo $twitter; ?>"><img src="<?php bloginfo('template_directory'); ?>/img/twitter.png" height="13" width="17" alt="twitter" /></a></li>

<?php } ?>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$facebook = get_option_tree( 'vn_facebook' );
} ?>

<?php if (isset($facebook)) { ?>

<li><a href="<?php echo $facebook; ?>"><img src="<?php bloginfo('template_directory'); ?>/img/facebook.png" height="18" width="9" alt="facebook" /></a></li>

<?php } ?>

</ul>

</div><!-- End of follow div -->

</li>

</ul>

<?php } else { } ?>
        
</div><!-- End of textwidget -->

</div><!-- End of one half first -->

</div><!-- End of map div -->

<!-- Start of map div2 -->
<div id="map_div2">

<!-- Start of one half -->
<div class="one_half">

<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('contact map')) : ?>
<?php endif; ?>

</div><!-- End of one half -->

<!-- ****************************THIS IS THE END OF THE CONTACT MAP WIDGET & DYNAMIC CONTACT DETAILS FROM ADMIN**************************** -->

<!-- Start of clear fix --><div class="clear"></div>

</div><!-- End of map div2 -->

<!-- Start of main fullwidth wrapper -->
<div id="main_fullwidth_wrapper">
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

<!-- Start of blog wrapper -->
<article class="blog_wrapper">

<!-- Start of clear fix --><div class="clear"></div>

<!-- Start of featured text full -->
<div class="featured_text_full">
<?php the_content('        '); ?> 

<?php endwhile; ?> 

<?php else: ?> 
<p>There are no posts to display. Try using the search.</p> 

<?php endif; ?>

</div><!-- End of featured text full -->

</article><!-- End of blog wrapper -->

</div><!-- End of main fullwidth wrapper -->

<!-- Start of clear fix --><div class="clear"></div>
            
</section><!-- End of main -->

<?php get_footer (); ?>