<?php
if ( function_exists('register_sidebars') ) {
register_sidebar(array(
	'name' => __( 'Home Side Widget', 'nature' ),
	'id' => 'home_side',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3 class="home">',
    'after_title' => '</h3>'
));
register_sidebar(array(
	'name' => __( 'Home Newsletter Widget', 'nature' ),
	'id' => 'home_news',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));
register_sidebar(array(
	'name' => __( 'Page', 'nature' ),
	'id' => 'page',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));
register_sidebar(array(
	'name' => __( 'Blog', 'nature' ),
	'id' => 'blog',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));
register_sidebar(array(
	'name' => __( 'Contact Map', 'nature' ),
	'id' => 'map',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));
register_sidebar(array(
	'name' => __( 'Footer One', 'nature' ),
	'id' => 'footer_one',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h6>',
    'after_title' => '</h6>'
));
register_sidebar(array(
	'name' => __( 'Footer Two', 'nature' ),
	'id' => 'footer_two',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h6>',
    'after_title' => '</h6>'
));
register_sidebar(array(
	'name' => __( 'Footer Three', 'nature' ),
	'id' => 'footer_three',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h6>',
    'after_title' => '</h6>'
));
register_sidebar(array(
	'name' => __( 'Footer Four', 'nature' ),
	'id' => 'footer_four',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h6>',
    'after_title' => '</h6>'
));
}
?>