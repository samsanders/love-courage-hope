<?php
/*
Template Name: Home
*/

get_header ();
?>



<!-- ******************************************************************** This is the slider ********************************************************************-->

<!-- Start of slider wrapper -->
<section class="slider_wrapper">

<!-- Start of slider -->
<section class="slider">   

<ul class="slides">

	<?php
    $temp = $wp_query;
    $wp_query = null;
    $wp_query = new WP_Query('post_type=slider&showposts=10');
    $wp_query->query('post_type=slider&showposts=10');
    ?>
        
    <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); sltws_post_meta(); ?>
        
        <li>
        <a href="<?php echo $meta[ 'subtitle' ]; ?>" target="_new" title="<?php the_title(); ?>"><?php the_post_thumbnail('slide'); ?></a>
        </li>
        
        <?php endwhile; ?>
        
	</ul>
    
    <?php wp_reset_query(); ?>

</section><!-- End of slider -->

<!-- Start of clear fix --><div class="clear"></div>

</section><!-- End of slider wrapper -->








<!-- ******************************************************************** This is the message area under the slider ********************************************************************-->

<!-- Start of main section home -->
<section id="main_section_home">

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$underslidertitle = get_option_tree( 'vn_underslidertitle' );
$underslidertext = get_option_tree( 'vn_underslidertext' );
$undersliderbuttontext = get_option_tree( 'vn_undersliderbuttontext' );
$undersliderbuttonlink = get_option_tree( 'vn_undersliderbuttonlink' );
} ?>

<!-- Start of message center -->
<section id="message_center">

<!-- Start of message center left -->
<div class="message_center_left">

<?php if ($underslidertitle != ('')){ ?> 
<h1 class="homepage"><?php echo stripslashes($underslidertitle); ?></h1>
<?php } else { } ?>

<?php if ($underslidertext != ('')){ ?> 
<p><?php echo stripslashes($underslidertext); ?></p>
<?php } else { } ?>

<?php if ($undersliderbuttontext != ('')){ ?> 

<!-- Start of under slider button -->
<div class="under_slider_button">
<a href="<?php echo ($undersliderbuttonlink); ?>"><?php echo stripslashes($undersliderbuttontext); ?></a>

</div><!-- End of under slider button -->

<?php } else { } ?>

</div><!-- End of message center left -->

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$donatetitle = get_option_tree( 'vn_donatetitle' );
$goalamountset = get_option_tree( 'vn_goalamountset' );
$goalpercentage = get_option_tree( 'vn_goalpercentage' );
$donatesmallertext = get_option_tree( 'vn_donatesmallertext' );
$donateamount = get_option_tree( 'vn_donateamount' );
$donatebuttontext = get_option_tree( 'vn_donatebuttontext' );
$donatebuttonlink = get_option_tree( 'vn_donatebuttonlink' );
} ?>

<!-- Start of message center right -->
<div class="message_center_right">

<?php if ($donatetitle != ('')){ ?> 

<!-- Start of donatetitle -->
<div class="donatetitle">
<?php echo stripslashes($donatetitle); ?>

</div><!-- End of donatetitle -->
<?php } else { } ?>

<?php if ($goalamountset != ('')){ ?> 

<!-- Start of donateamount -->
<div class="donateamount">
<?php echo stripslashes($goalamountset); ?>

</div><!-- End of donateamount -->
<?php } else { } ?>

<?php
if ($goalpercentage) { ?>

<!-- Start of meterwrapper -->
<div class="meterwrapper">

<!-- Start of meter -->
<div class="meter">
<span style="width:<?php echo ($goalpercentage); ?>%"></span>

</div><!-- End of meter -->

</div><!-- End of meterwrapper -->

<?php } else { } ?>

<?php if ($donatesmallertext != ('')){ ?> 

<!-- Start of donatetitle -->
<div class="donatetitle">
<?php echo stripslashes($donatesmallertext); ?>

</div><!-- End of donatetitle -->
<?php } else { } ?>

<?php if ($donateamount != ('')){ ?> 

<!-- Start of donateamount -->
<div class="donateamount">
<?php echo stripslashes($donateamount); ?>
<?php } else { } ?>

<?php if ($donatebuttontext != ('')){ ?> 
<a class="donatebutton" href="<?php echo ($donatebuttonlink); ?>"><?php echo stripslashes($donatebuttontext); ?></a>
<?php } else { } ?>

</div><!-- End of donateamount -->

</div><!-- End of message center right -->

<!-- Start of clear fix --><div class="clear"></div>

</section><!-- End of message center -->

</section><!-- End of main section home -->

<!-- Start of main -->
<section id="main">





<!-- ******************************************************************** This is the blog loop under the message center  - left side ********************************************************************-->





<!-- Start of message center left -->
<div class="message_center_left">

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$featured_number = get_option_tree( 'vn_selectnumberblog' );
} ?>

<?php 
$featured = new WP_Query('&showposts=' . $featured_number);

while ($featured->have_posts()) : $featured->the_post();
?>

<?php get_template_part( 'content', get_post_format() ); ?>

<hr />

<?php endwhile; ?>
			
<?php wp_reset_query(); ?>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$morenewstext = get_option_tree( 'vn_morenewstext' );
$morenewslink = get_option_tree( 'vn_morenewslink' );
} ?>

<?php if ($morenewstext != ('')){ ?> 
<a class="forward" href="<?php echo ($morenewslink); ?>"><?php echo stripslashes($morenewstext); ?></a>
<?php } else { } ?>

</div><!-- End of message center left -->






<!-- ******************************************************************** This is event loop - right side ********************************************************************-->





<!-- Start of message center right light -->
<div class="message_center_right_light">

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$eventlooptitle = get_option_tree( 'vn_eventlooptitle' );
} ?>

<?php if ($eventlooptitle != ('')){ ?>

<!-- Start of event title -->
<div class="event_title">
<?php echo ($eventlooptitle); ?>

</div><!-- End of event title -->

<?php } else { } ?>

<?php
$featuredevent = new WP_Query('post_type=event&showposts=3');
while ($featuredevent->have_posts()) : $featuredevent->the_post();
?> 

<?php
$eventdate = get_post_meta($post->ID, 'eventdate', $single = true);   
?>

<h3 class="event"><?php the_title (); ?></h3>

<!-- Start of post content last -->
<div class="post_content_last">
<?php echo ($eventdate); ?>

</div><!-- End of post content last -->

<!-- Start of clear fix --><div class="clear"></div>

<!-- Start of home event text -->
<div class="home_event_text">
<p><?php $excerpt = get_the_excerpt(); echo string_limit_words($excerpt,20); ?></p>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$readmoretext = get_option_tree( 'vn_readmore' );
} ?>

<a class="forward" href="<?php the_permalink(); ?>"><?php echo stripslashes($readmoretext); ?></a>

</div><!-- End of home event text -->

<!-- Start of line break --><div class="hrr"></div>

<?php endwhile; ?>
			
<?php wp_reset_query(); ?>


<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('home_side') ) : else : ?>		
<?php endif; ?>

</div><!-- End of message center right -->

<!-- Start of clear fix --><div class="clear"></div>
            
</section><!-- End of main -->

<?php get_footer(); ?>