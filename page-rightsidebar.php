<?php
/*
Template Name: Right-Sidebar
*/
?>

<?php get_header(); ?>

<!-- Start of main -->
<section id="main">

<!-- Start of message center left -->
<div class="message_center_left">
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

<!-- Start of blog wrapper -->
<article class="blog_wrapper">

<!-- Start of featured text full -->
<div class="featured_text_full">

<?php the_content('        '); ?> 

<?php endwhile; ?> 

<?php else: ?> 
<p><?php _e( 'There are no posts to display. Try using the search.', 'nature' ); ?></p> 

<?php endif; ?>

</div><!-- End of featured text full -->

<!-- Start of clear fix --><div class="clear"></div>

</article><!-- End of blog wrapper -->

</div><!-- End of message center left -->

<!-- Start of message center right light -->
<div class="message_center_right_light">

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$eventlooptitle = get_option_tree( 'vn_eventlooptitle' );
} ?>

<?php if ($eventlooptitle != ('')){ ?>

<!-- Start of event title -->
<div class="event_title">
<?php echo ($eventlooptitle); ?>

</div><!-- End of event title -->

<?php } else { } ?>

<?php
$featuredevent = new WP_Query('post_type=event&showposts=3');
while ($featuredevent->have_posts()) : $featuredevent->the_post();
?> 

<?php
$eventdate = get_post_meta($post->ID, 'eventdate', $single = true);   
?>

<h3 class="event"><?php the_title (); ?></h3>

<!-- Start of post content last -->
<div class="post_content_last">
<?php echo ($eventdate); ?>

</div><!-- End of post content last -->

<!-- Start of clear fix --><div class="clear"></div>

<!-- Start of home event text -->
<div class="home_event_text">
<p><?php $excerpt = get_the_excerpt(); echo string_limit_words($excerpt,20); ?></p>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$readmoretext = get_option_tree( 'vn_readmore' );
} ?>

<a class="forward" href="<?php the_permalink(); ?>"><?php echo stripslashes($readmoretext); ?></a>

</div><!-- End of home event text -->

<!-- Start of line break --><div class="hrr"></div>

<?php endwhile; ?>
			
<?php wp_reset_query(); ?>


<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('home_side') ) : else : ?>		
<?php endif; ?>

</div><!-- End of message center right -->

<!-- Start of clear fix --><div class="clear"></div>
            
</section><!-- End of main -->

<?php get_footer (); ?>
