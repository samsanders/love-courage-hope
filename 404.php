<?php $path = get_template_directory_uri();
if(!isset($_REQUEST['error']))  $error_code = '404';
else  $error_code = $_REQUEST['error'];
?>

<?php get_header(); ?>

<!-- Start of main -->
<section id="main">

<!-- Start of main fullwidth wrapper -->
<div id="main_fullwidth_wrapper">

<!-- Start of blog wrapper -->
<article class="blog_wrapper">
<h1><?php _e( 'Page Not Found', 'nature' ); ?></h1>

<!-- Start of featured text full -->
<div class="featured_text_full">
<p><?php _e( 'No worries you probably mistyped something, looked for something that does not exist anymore or just downright broke stuffs - just carry on with the menu at the top!', 'nature' ); ?></p>

</div><!-- End of featured text full -->

<!-- Start of clear fix --><div class="clear"></div>

</article><!-- End of blog wrapper -->

</div><!-- End of main fullwidth wrapper -->

<!-- Start of clear fix --><div class="clear"></div>
            
</section><!-- End of main -->

<?php get_footer (); ?>