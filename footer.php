<!-- Start of home newsletter -->
<div class="homenewsletter">
<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('home_news') ) : else : ?>		
<?php endif; ?>   
    
<?php // echo do_shortcode( '[contact-form-7 id="88" title="Newsletter"]' ); ?>     
    
<div id="wufoo-z7p9p1">
</div>
<script type="text/javascript">var z7p9p1;(function(d, t) {
var s = d.createElement(t), options = {
'userName':'bastionaus', 
'formHash':'z7p9p1', 
'autoResize':true,
'height':'261',
'async':true,
'header':'show', 
'ssl':false};
s.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + 'wufoo.com/scripts/embed/form.js';
s.onload = s.onreadystatechange = function() {
var rs = this.readyState; if (rs) if (rs != 'complete') if (rs != 'loaded') return;
try { z7p9p1 = new WufooForm();z7p9p1.initialize(options);z7p9p1.display(); } catch (e) {}};
var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr);
})(document, 'script');</script>  


<!-- Start of clear fix --><div class="clear"></div>

</div><!-- End of home newsletter -->

</div><!-- End of bdywrapper -->

<!-- Start of top -->
<div id="top"> 

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$footerimage = get_option_tree( 'vn_footerimage' );
} ?>

<?php if (isset($footerimage)) { ?>

<!-- Start of main section bg -->
<div id="main_section_bg" style="background-image:url(<?php echo $footerimage; ?>); background-repeat:repeat-x; height:300px;">


</div><!-- End of main section bg -->

<!-- Start of clear fix --><div class="clear"></div>

<?php } ?>

</section><!-- End of main section -->

</div><!-- End of top -->

<!-- Start of outer footer wrapper -->
<footer id="outer_footer_wrapper">

<!-- Start of footer wrapper -->
<div id="footer_wrapper">

<!-- Start of one fourth first -->
<!--<div class="one_fourth_first">-->

<!--<a href="<?php // bloginfo('siteurl'); ?>">--><?php 
// if ( function_exists( 'get_option_tree' ) ) {
// $bottomlogo = get_option_tree( 'vn_bottomlogo' );
// } ?><!--<img src="--><?php // echo $bottomlogo; ?><!-- " alt="bottom logo" /></a> -->

<?php // if (function_exists('dynamic_sidebar') && dynamic_sidebar('footer_one') ) : else : ?>		
<?php // endif; ?>

<!--</div> end of one fourth first -->

<!-- Start of one fourth -->
<div class="one_fourth footer-contact">
The Love Courage Hope Foundation, P.O. Box 160, Carnegie, Vic 3163
</div><!-- end of one fourth -->

<!-- Start of one fourth -->
<div class="one_fourth footer-email">
    <a href="mailto:lovecouragehope@bigpond.com.au">lovecouragehope@bigpond.com.au</a>
</div> <!-- end of one fourth -->

<!-- Start of one fourth -->
<!--<div class="one_fourth">-->
<?php // if (function_exists('dynamic_sidebar') && dynamic_sidebar('footer_four') ) : else : ?>		
<?php // endif; ?>

<!--</div> end of one fourth -->

</div><!-- End of footer wrapper -->

<div id="footer-sponsors">
    <h4>Major Sponsors</h4>
    <ul>
        <li><a href="http://bastion.com.au/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/bastion.png" alt="Sponsor - Bastion" /></a></li>
        
    </ul>
</div>

<!-- Start of clear fix --><div class="clear"></div>

</footer><!-- End of outer footer wrapper -->

<!-- Start of bottom nav wrap -->
<div id="bottom_nav_wrap">

<!-- Start of copyright message -->
<div class="copyright_message">
<?php 
if ( function_exists( 'get_option_tree' ) ) {
$copyright = get_option_tree( 'vn_copyright' );
} ?>     
 
&copy;<?php echo stripslashes($copyright); ?>

</div><!-- End of copyright message -->

<!-- Start of social -->
<section class="social">

<ul class="icons">

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$pinterest = get_option_tree( 'vn_pinterest' );
} ?>

<?php if (isset($pinterest)) { ?>

<li><a href="<?php echo $pinterest; ?>"><img src="<?php bloginfo('template_directory'); ?>/img/pinterest.png" height="18" width="13" alt="pinterest" /></a></li>

<?php } ?>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$flickrlink = get_option_tree( 'vn_flickr' );
} ?>

<?php if (isset($flickrlink)) { ?>

<li><a href="<?php echo $flickrlink; ?>"><img src="<?php bloginfo('template_directory'); ?>/img/flickr.png" height="18" width="20" alt="flickr" /></a></li>

<?php } ?>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$googlelink = get_option_tree( 'vn_googlelink' );
} ?>

<?php if (isset($googlelink)) { ?>

<li><a href="<?php echo $googlelink; ?>"><img src="<?php bloginfo('template_directory'); ?>/img/googleplus.png" height="15" width="16" alt="google plus" /></a></li>

<?php } ?>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$twitter = get_option_tree( 'vn_twitter' );
} ?>

<?php if (isset($twitter)) { ?>

<li><a href="<?php echo $twitter; ?>"><img src="<?php bloginfo('template_directory'); ?>/img/twitter.png" height="13" width="17" alt="twitter" /></a></li>

<?php } ?>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$facebook = get_option_tree( 'vn_facebook' );
} ?>

<?php if (isset($facebook)) { ?>

<li><a href="<?php echo $facebook; ?>"><img src="<?php bloginfo('template_directory'); ?>/img/facebook.png" height="18" width="9" alt="facebook" /></a></li>

<?php } ?>

</ul>

</section><!-- End of social -->

</div><!-- end of bottom nav wrap -->


<div class="clear"></div>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$analytics = get_option_tree( 'vn_google_analytics' );
} ?>     

<?php echo stripslashes($analytics); ?>

<script type="text/javascript">
jQuery(function() {
   jQuery('#da-thumbs > li').hoverdir();
});
</script>

</body>
</html>

<?php wp_footer(); ?>