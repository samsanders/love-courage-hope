<?php  
/* 
Template Name: Sitemap 
*/  
?>

<?php get_header(); ?>	

<!-- Start of main -->
<section id="main">

<!-- Start of message center left -->
<div class="message_center_left">

<!-- Start of blog wrapper -->
<article class="blog_wrapper">
<h1><?php _e( 'Pages', 'nature' ); ?></h1>

<!-- Start of featured_text blog -->
<div class="featured_text_full">

<ul><?php wp_list_pages("title_li=" ); ?></ul>

</div><!-- End of featured_text blog -->  

</article><!-- End of blog wrapper -->

<hr />

<!-- Start of blog wrapper -->
<article class="blog_wrapper">
<h1><?php _e( 'Feeds', 'nature' ); ?></h1>

<!-- Start of featured_text blog -->
<div class="featured_text_full">

<ul>

<li><a title="<?php _e( 'Full content', 'nature' ); ?>" href="feed:<?php bloginfo('rss2_url'); ?>"><?php _e( 'Main RSS', 'nature' ); ?></a></li>
<li><a title="<?php _e( 'Comment Feed', 'nature' ); ?>" href="feed:<?php bloginfo('comments_rss2_url'); ?>"><?php _e( 'Comment Feed', 'nature' ); ?></a></li>

</ul>

</div><!-- End of featured_text blog -->  

</article><!-- End of blog wrapper -->

<hr />

<!-- Start of blog wrapper -->
<article class="blog_wrapper">
<h1><?php _e( 'Categories', 'nature' ); ?></h1>

<!-- Start of featured_text blog -->
<div class="featured_text_full">

<ul>

<?php $args = array(
    'show_option_all'    => '',
    'orderby'            => 'name',
    'order'              => 'ASC',
    'style'              => 'list',
    'show_count'         => 1,
    'hide_empty'         => 1,
    'use_desc_for_title' => 0,
    'child_of'           => 0,
    'feed'               => '',
    'feed_type'          => '',
    'feed_image'         => '',
    'exclude'            => '',
    'exclude_tree'       => '',
    'include'            => '',
    'hierarchical'       => true,
    'title_li'           => '',
    'show_option_none'   => __('No categories'),
    'number'             => NULL,
    'echo'               => 1,
    'depth'              => 0,
    'current_category'   => 0,
    'pad_counts'         => 0,
    'taxonomy'           => 'category',
    'walker'             => 'Walker_Category' ); ?> 
	
	<?php wp_list_categories( $args ); ?>
    
</ul>

</div><!-- End of featured_text blog -->  

</article><!-- End of blog wrapper -->

<hr />

<!-- Start of blog wrapper -->
<article class="blog_wrapper">
<h1><?php _e( 'All Blog Posts', 'nature' ); ?></h1>

<!-- Start of featured_text blog -->
<div class="featured_text_full">

<ul>

<?php $archive_query = new WP_Query('showposts=1000&cat=-8');
while ($archive_query->have_posts()) : $archive_query->the_post(); ?>

<li>
<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e( 'Permanent Link to', 'nature' ); ?><?php the_title(); ?>"><?php the_title(); ?></a>
(<?php comments_number('0', '1', '%'); ?>)
</li>

<?php endwhile; ?>
    
</ul>

</div><!-- End of featured_text blog -->  

</article><!-- End of blog wrapper -->

<hr />

<!-- Start of blog wrapper -->
<article class="blog_wrapper">
<h1><?php _e( 'Archives', 'nature' ); ?></h1>

<!-- Start of featured_text blog -->
<div class="featured_text_full">

<ul>

<?php wp_get_archives('type=monthly&show_post_count=true'); ?>
    
</ul>

</div><!-- End of featured_text blog -->  

</article><!-- End of blog wrapper -->

<hr />

</div><!-- End of message center left -->

<!-- Start of blog right light -->
<div class="blog_right_light">
<?php get_sidebar ('page'); ?>            

</div><!-- End of blog right light -->

<!-- Start of clear fix --><div class="clear"></div>
            
</section><!-- End of main -->

<?php get_footer (); ?>