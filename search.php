<?php get_header(); ?>

<!-- Start of main -->
<section id="main">
<h1>Results for ' <?php echo($s); ?> '</h1>

<!-- Start of message center left -->
<div class="message_center_left">
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

<!-- Start of blog wrapper -->
<article class="blog_wrapper">

<!-- Start of line break --><div class="hrr"></div>

<!-- Start of posted details -->
<div class="posted_details">

<div class="authorpic"></div>

<!-- Start of post content first -->
<div class="post_content_first">
<?php the_author() ?>

</div><!-- End of post content first -->

<div class="datepic"></div>

<!-- Start of post content -->
<div class="post_content">
<?php the_time('F jS, Y') ?>

</div><!-- End of post content -->

<?php
if (has_tag()) { ?>

<div class="tagpic"></div>

<!-- Start of post content -->
<div class="post_content">
<?php the_tags(__(''), ', '); ?>

</div><!-- End of post content -->

<?php } else { }?>

</div><!-- End of posted details -->

<br />

<!-- Start of line break --><div class="hrr"></div>   

<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

<!-- Start of featured text full -->
<div class="featured_text_full">
<p><?php $excerpt = get_the_excerpt(); echo string_limit_words($excerpt,55); ?></p>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$readmoretext = get_option_tree( 'vn_readmore' );
} ?>

<a class="forward" href="<?php the_permalink(); ?>"><?php echo stripslashes($readmoretext); ?></a>

</div><!-- End of featured text full -->

<!-- Start of clear fix --><div class="clear"></div>

</article><!-- End of blog wrapper -->

<hr />

<div class="big"></div>
        
<?php endwhile; ?> 
            
<?php else: ?> 
	<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'nature' ); ?></p> 
<?php endif; ?> 

<div class="clear"></div>

<!-- Start of navigation -->
<div class="navigation">

<!-- Start of alignleft -->
<div class="alignleft">
<?php next_posts_link( __('Older','nature') ) ?>

</div><!-- End of alignleft -->

<!-- Start of alignright -->
<div class="alignright">
<?php previous_posts_link( __('Newer', '', 'yes') ) ?> 

</div><!-- End of alignright -->

</div><!-- End of navigation -->  

</div><!-- End of message center left -->

<!-- Start of blog right light -->
<div class="blog_right_light">
<?php get_sidebar ('page'); ?>            

</div><!-- End of blog right light -->  
            
</section><!-- End of main -->

<div class="clear"></div>

<div style="height:60px;"></div>

<?php get_footer (); ?>
