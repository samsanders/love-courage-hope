<?php
/*
Template Name: Event-Page
*/
?>

<?php get_header(); ?>

<!-- Start of main -->
<section id="main">

<!-- Start of main fullwidth wrapper -->
<div id="main_fullwidth_wrapper">
<?php
$temp = $wp_query;
$wp_query = null;
$wp_query = new WP_Query();
$wp_query->query('post_type=event' . '&paged=' . $paged . '');
?>
	
<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

<!-- Start of event block -->
<section class="eventblock">

<!-- Start of event block title -->
<div class="eventblocktitle">
<h1><?php the_title(); ?></h1>

</div><!-- End of event block title -->

<!-- Start of event block content -->
<div class="eventblockcontent">

<?php
$eventaddress = get_post_meta($post->ID, 'eventaddress', $single = true); 
$eventcost = get_post_meta($post->ID, 'eventcost', $single = true); 
$eventdate = get_post_meta($post->ID, 'eventdate', $single = true); 
$eventtime = get_post_meta($post->ID, 'eventtime', $single = true); 
$eventmaplink = get_post_meta($post->ID, 'eventmaplink', $single = true);   
?>

<ul class="event">
<?php if ($eventaddress != ('')){ ?> 
<li>

<!-- Start of event address -->
<div class="eventaddress">
<div class="mappic"></div><?php echo stripslashes($eventaddress); ?> 

</div><!-- End of event address -->

</li>

<?php } else { } ?>

<?php if ($eventdate != ('')){ ?> 

<li>

<!-- Start of event date -->
<div class="eventdate">
<div class="datepic"></div><?php echo stripslashes($eventdate); ?> 

</div><!-- End of event date -->

</li>

<?php } else { } ?>

<?php if ($eventcost != ('')){ ?> 

<li>

<!-- Start of event cost -->
<div class="eventcost">
<div class="costpic"></div><?php echo stripslashes($eventcost); ?> 

</div><!-- End of event cost -->

</li>

<?php } else { } ?>

<?php if ($eventmaplink != ('')){ ?> 

<li>

<!-- Start of map link -->
<div class="maplink">
<div class="directionspic"></div><a href="<?php echo ($eventmaplink); ?>" target="_new"><?php _e( 'View map / Get Directions', 'nature' ); ?></a>

</div><!-- End of map link -->

</li>

<?php } else { } ?>

</ul>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$readmoretext = get_option_tree( 'vn_readmore' );
} ?>

<!-- Start of read more button -->
<div class="button">

<a href="<?php the_permalink(); ?>"><?php echo stripslashes($readmoretext); ?></a>

<!-- Start of clear fix --><div class="clear"></div>

</div><!-- End of read more button -->

</div><!-- End of event block content -->

<!-- Start of event social share wrapper -->
<div class="event_social_share_wrapper">

<!-- Start of social share links -->
<div class="social_share_links">

<div class="socialpic"></div>
<a class="socialsharing" target="_blank" href="http://www.facebook.com/share.php?u=<?php the_permalink (); ?>"><?php _e( 'facebook', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="https://plus.google.com/share?url=<?php the_permalink (); ?>"><?php _e( 'google', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="http://twitter.com/home?status=<?php the_permalink (); ?>"><?php _e( 'twitter', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php the_permalink (); ?>"><?php _e( 'pinterest', 'nature' ); ?></a>
        
</div><!-- End of social share links -->

</div><!-- End of event social share wrapper -->

</section><!-- End of event block -->

<?php endwhile; ?> 

<div class="clear"></div>

<div class="big"></div>

<hr />

<!-- Start of navigation -->
<div class="navigation">

<!-- Start of alignleft -->
<div class="alignleft">
<?php next_posts_link( __('Older','nature') ) ?>

</div><!-- End of alignleft -->

<!-- Start of alignright -->
<div class="alignright">
<?php previous_posts_link( __('Newer', '', 'yes') ) ?> 

</div><!-- End of alignright -->

</div><!-- End of navigation -->  

</div><!-- End of main fullwidth wrapper -->

<!-- Start of clear fix --><div class="clear"></div>
            
</section><!-- End of main -->

<?php get_footer (); ?>