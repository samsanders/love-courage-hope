<?php
/*
Template Name: Photos
*/
?>

<?php get_header(); ?>

<!-- Start of main -->
<section id="main">

<!-- Start of main fullwidth wrapper -->
<div id="main_fullwidth_wrapper">
<?php
$temp = $wp_query;
$wp_query = null;
$wp_query = new WP_Query();
$wp_query->query('post_type=portgallery' . '&paged=' . $paged . '&posts_per_page=600000000');
?>

<!-- Start of portgallery wrapper -->
<article class="portgallery_wrapper">

<ul id="da-thumbs" class="da-thumbs">
<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

<?php
$portgallerylink = get_post_meta($post->ID, 'portgallerylink', $single = true);   
?>

<li class="box">
<a href="<?php echo ($portgallerylink); ?>">
<?php the_post_thumbnail('large'); ?>
<div><span><?php the_title (); ?></span></div>
</a>
</li>
<?php endwhile; ?> 
</ul>

</article><!-- End of portgallery wrapper -->

<div class="clear"></div>

<div class="big"></div>

<hr />

</div><!-- End of main fullwidth wrapper -->

<!-- Start of clear fix --><div class="clear"></div>
            
</section><!-- End of main -->

<?php wp_reset_query(); ?>

<?php get_footer (); ?>