<?php
/*
 * The default template for displaying audio
 */
?>

<!-- Start of blog wrapper -->
<article class="blog_wrapper">

<!-- Start of line break --><div class="hrr"></div>

<!-- Start of posted details -->
<div class="posted_details">

<div class="authorpic"></div>

<!-- Start of post content first -->
<div class="post_content_first">
<?php the_author() ?>

</div><!-- End of post content first -->

<div class="datepic"></div>

<!-- Start of post content -->
<div class="post_content">
<?php the_time('F jS, Y') ?>

</div><!-- End of post content -->

<?php
if (has_tag()) { ?>

<div class="tagpic"></div>

<!-- Start of post content -->
<div class="post_content">
<?php the_tags(__(''), ', '); ?>

</div><!-- End of post content -->

<?php } else { }?>

<!-- Start of post content last -->
<div class="post_content_last">
<?php if ('open' == $post->comment_status) { ?>
<?php comments_popup_link('0', '1', '%', 'comments-link'); ?>
<?php } ?>

</div><!-- End of post content last -->

</div><!-- End of posted details -->

<br />

<!-- Start of line break --><div class="hrr"></div>   

<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

<!-- Start of featured text blog -->
<div class="featured_text_full">
<p><?php the_content(); ?></p>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$readmoretext = get_option_tree( 'vn_readmore' );
} ?>

<a class="forward" href="<?php the_permalink(); ?>"><?php echo stripslashes($readmoretext); ?></a>

<!-- Start of social share wrapper -->
<div class="social_share_wrapper">

<div class="socialpic"></div>

<!-- Start of social share links -->
<div class="social_share_links">
<a class="socialsharing" target="_blank" href="http://www.facebook.com/share.php?u=<?php the_permalink (); ?>"><?php _e( 'facebook', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="https://plus.google.com/share?url=<?php the_permalink (); ?>"><?php _e( 'google', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="http://twitter.com/home?status=<?php the_permalink (); ?>"><?php _e( 'twitter', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php the_permalink (); ?>"><?php _e( 'pinterest', 'nature' ); ?></a>
        
</div><!-- End of social share links -->

</div><!-- End of social share wrapper -->

</div><!-- End of featured text blog -->

<!-- Start of clear fix --><div class="clear"></div>      

</article><!-- End of blog wrapper -->