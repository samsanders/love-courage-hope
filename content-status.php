<?php
/*
 * The default template for displaying status
 */
?>

<!-- Start of blog wrapper -->
<article class="blog_wrapper">

<!-- Start of featured text -->
<div class="featured_text_full">
<?php the_content(); ?>

</div><!-- End of featured text -->

<!-- Start of posted details -->
<div class="posted_details">

<div class="datepic"></div>

<!-- Start of post content -->
<div class="post_content">
<?php the_time('F jS, Y') ?>

</div><!-- End of post content -->

<!-- Start of social share wrapper -->
<div class="social_share_wrapper">

<div class="socialpic"></div>

<!-- Start of social share links -->
<div class="social_share_links">
<a class="socialsharing" target="_blank" href="http://www.facebook.com/share.php?u=<?php the_permalink (); ?>"><?php _e( 'facebook', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="https://plus.google.com/share?url=<?php the_permalink (); ?>"><?php _e( 'google', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="http://twitter.com/home?status=<?php the_permalink (); ?>"><?php _e( 'twitter', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php the_permalink (); ?>"><?php _e( 'pinterest', 'nature' ); ?></a>
        
</div><!-- End of social share links -->

</div><!-- End of social share wrapper -->

</div><!-- End of posted details -->

<!-- Start of clear fix --><div class="clear"></div>

</article><!-- End of blog wrapper -->
