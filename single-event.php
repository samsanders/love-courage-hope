<?php get_header(); ?>

<!-- Start of main -->
<section id="main">
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

<!-- Start of main fullwidth wrapper -->
<section id="main_fullwidth_wrapper">
<?php the_post_thumbnail('slide'); ?>

<!-- Start of blog wrapper -->
<article class="blog_wrapper">         
<h1><?php the_title(); ?></h1>
<br />

<!-- Start of map div -->
<div id="map_div3">

<!-- ****************************THIS IS THE START OF THE CONTACT MAP WIDGET & DYNAMIC CONTACT DETAILS FROM ADMIN**************************** -->

<!-- Start of one half first -->
<div class="one_half_first">

<?php
$eventaddress = get_post_meta($post->ID, 'eventaddress', $single = true); 
$eventcost = get_post_meta($post->ID, 'eventcost', $single = true); 
$eventdate = get_post_meta($post->ID, 'eventdate', $single = true); 
$eventtime = get_post_meta($post->ID, 'eventtime', $single = true); 
$eventmaplink = get_post_meta($post->ID, 'eventmaplink', $single = true);  
$eventmap = get_post_meta($post->ID, 'eventmap', $single = true);   
?>

<!-- Start of textwidget -->
<div class="textwidget">

<!-- ****************************THIS IS THE START OF THE CONTACT INFORMATION**************************** -->

<?php if ($eventaddress != ('')){ ?> 

<ul class="contact">

<li>
<div class="mappic"></div>

<!-- Start of address div -->
<div class="addressdiv">
<?php echo stripslashes($eventaddress); ?>

</div><!-- End of address div -->

</li>

<?php } else { } ?>

<?php if ($eventdate != ('')){ ?> 

<li>
<div class="datepic"></div>

<!-- Start of phone div -->
<div class="phonediv">
<?php echo stripslashes($eventdate); ?> <br />
<?php echo stripslashes($eventtime); ?>

</div><!-- End of phone div -->

</li>

<?php } else { } ?>

<?php if ($eventcost != ('')){ ?> 

<li>
<div class="costpic"></div>

<!-- Start of fax div -->
<div class="faxdiv">
<?php echo stripslashes($eventcost); ?> 

</div><!-- End of fax div -->

</li>

<?php } else { } ?>

<?php if ($eventmaplink != ('')){ ?> 

<li>
<div class="directionspic"></div>

<!-- Start of email div -->
<div class="emaildiv">
<a href="<?php echo ($eventmaplink); ?>" target="_new"><?php _e( 'View map / Get Directions', 'nature' ); ?></a>

</div><!-- End of email div -->

</li>

<?php } else { } ?>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$facebook = get_option_tree( 'vn_facebook' );
} ?>

<?php if (isset($facebook)) { ?>

<li>
<div class="socialpic"></div>

<!-- Start of social share wrapper -->
<div class="social_share_wrapper3">

<!-- Start of social share links -->
<div class="social_share_links">
<a class="socialsharing" target="_blank" href="http://www.facebook.com/share.php?u=<?php the_permalink (); ?>"><?php _e( 'facebook', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="https://plus.google.com/share?url=<?php the_permalink (); ?>"><?php _e( 'google', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="http://twitter.com/home?status=<?php the_permalink (); ?>"><?php _e( 'twitter', 'nature' ); ?></a>

<a class="socialsharing" target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php the_permalink (); ?>"><?php _e( 'pinterest', 'nature' ); ?></a>
        
</div><!-- End of social share links -->

</div><!-- End of social share wrapper -->

</li>

</ul>

<?php } else { } ?>
        
</div><!-- End of textwidget -->

</div><!-- End of one half first -->

</div><!-- End of map div -->

<!-- Start of map div2 -->
<div id="map_div2">

<!-- Start of one half -->
<div class="one_half">
<?php if ($eventmap != ('')){ ?> 
<?php echo stripslashes($eventmap); ?>
<?php } else { } ?>

</div><!-- End of one half -->

<!-- ****************************THIS IS THE END OF THE CONTACT MAP WIDGET & DYNAMIC CONTACT DETAILS FROM ADMIN**************************** -->

<!-- Start of clear fix --><div class="clear"></div>

</div><!-- End of map div2 -->

<div class="clear"></div>

<div style="height:35px;"></div>

<!-- Start of featured text blog -->
<div class="featured_text_full">

<?php the_content('        '); ?> 

<?php endwhile; ?> 

<?php else: ?> 
<p><?php _e( 'There are no posts to display. Try using the search.', 'nature' ); ?></p> 

<?php endif; ?>

</div><!-- End of featured text blog -->

</article><!-- End of blog wrapper -->

<!-- Start of clear fix --><div class="clear"></div>

</section><!-- End of main fullwidth wrapper -->

<div class="clear"></div>

<div style="height:60px;"></div>

<hr />

<!-- Start of navigation -->
<div class="navigation">

<!-- Start of alignleft -->
<div class="alignleft">
<?php next_post('%', '', 'yes'); ?>

</div><!-- End of alignleft -->

<!-- Start of alignright -->
<div class="alignright">
<?php previous_post('%', '', 'yes'); ?> 

</div><!-- End of alignright -->

<!-- Start of clear fix --><div class="clear"></div>

</div><!-- End of navigation -->  

<!-- Start of clear fix --><div class="clear"></div>
            
</section><!-- End of main -->

<?php get_footer (); ?>