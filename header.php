<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<!--Google Fonts-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

<!--[if IE 7 ]>    <html class= "ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class= "ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class= "ie9"> <![endif]-->

<!--[if lt IE 9]>
   <script>
      document.createElement('header');
      document.createElement('nav');
      document.createElement('section');
      document.createElement('article');
      document.createElement('aside');
      document.createElement('footer');
   </script>
<![endif]--><head>
<title><?php echo get_option('blogname'); ?><?php wp_title(); ?></title>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="google-site-verification" content="l1AL-EZGOFy63_NduDNcypDlTCDAFAKFHP4jvq6grvM" />

<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>

<link rel="image_src" 
      type="image/jpeg" 
      href="http://lovecouragehope.bastionserve.net/wp-content/themes/love-courage-hope/img/lch_logo.png" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

 <!-- *************************************************************************
*****************                FAVICON               ********************
************************************************************************** -->
<?php 
if ( function_exists( 'get_option_tree') ) {
  $favicon = get_option_tree( 'vn_favicon' );
}
?>
<link rel="shortcut icon" href="<?php echo ($favicon); ?>" />

  <!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<?php wp_head(); ?>

 <!-- *************************************************************************
*****************        RESPONSIVE MENU SELECT        ********************
************************************************************************** -->
<script type="text/javascript">
// DOM ready
jQuery(document).ready(function(){

// Create the dropdown base
jQuery("<select />").appendTo("#topmenu");

// Create default option "Go to..."
jQuery("<option />", {
 "selected": "selected",
 "value"   : "",
 "text"    : "Menu Selection..."
}).appendTo("#topmenu select");

// Populate dropdown with menu items
jQuery("#topmenu a").each(function() {
var el = jQuery(this);
jQuery("<option />", {
   "value"   : el.attr("href"),
   "text"    : el.text()
}).appendTo("#topmenu select");
});

// To make dropdown actually work
// To make more unobtrusive: http://css-tricks.com/4064-unobtrusive-page-changer/
jQuery("#topmenu select").change(function() {
window.location = jQuery(this).find("option:selected").val();
});

});
</script>



 <!-- *************************************************************************
*****************              TWITTER FEED            ********************
************************************************************************** -->
<?php 
if ( function_exists( 'get_option_tree') ) {
  $twitter = get_option_tree( 'vn_twitter_username' );
}
?>
<script type='text/javascript'>
    jQuery(document).ready(function(){
        jQuery(".tweet").tweet({
            username: "<?php echo $twitter; ?>",
            join_text: "auto",
            avatar_size: 32,
            count: 3,
            auto_join_text_default: "we said,", 
            auto_join_text_ed: "we",
            auto_join_text_ing: "we were",
            auto_join_text_reply: "we replied to",
            auto_join_text_url: "we were checking out",
            loading_text: "loading tweets..."
        });
    });
</script>



<!-- *************************************************************************
******************   THIS IS THE SLIDER           ***********************
************************************************************************** -->
<script type="text/javascript">
jQuery(window).load(function() {
	jQuery('.slider').flexslider();
});
	
</script>


 <!-- *************************************************************************
*****************             ACCENT COLOR            ********************
************************************************************************** -->
<?php 
if ( function_exists( 'get_option_tree') ) {
  $accentcolor = get_option_tree( 'vn_accentcolor' );
}
?>

<style type="text/css">

h3 a:hover, h4 a:hover, h6 a:hover, a, a.more-link, a:hover, a:hover.more-link, .intro a, .intro a:hover, .buttontextnext a:hover, .comment-date a, .comment-date a:hover, .searchme, .forward, .forward2, h1 a:hover, .datepic, .post_content a:hover .post_content_first a:hover, .socialpic, a.socialsharing:hover, .authorpic, .tagpic, .alignright a:hover, .alignleft a:hover, .alignright a, .alignleft a, .back, .copyright_message a:hover, .tweet_time a, .tweet_time a:hover, .tweet_text a, ul.tweet_list li .tweet_text a:hover, ul li a, ol li a, .comment-date, .mappic, .phonepic, .faxpic, .emailpic, .followpic, .directionspic, .costpic, .blog_right_light ul li a:hover, .blog_right_light ol li a:hover,.blog_left_light ul li a:hover, .blog_left_light ol li a:hover, ul.tweet_list li a, .portgallerpic {
	color:<?php echo ($accentcolor); ?>;
	}
	
input[type=submit], .button a, .button_reverse a:hover, .message_center_right, .meter, .post_content_last, #outer_footer_wrapper, .under_slider_button, .eventblocktitle {
	background-color:<?php echo ($accentcolor); ?>;
	}
	
#searchbox input:focus, input[type=text]:focus, .homenewsletter input[type=text]:focus, textarea:focus {
	border:1px solid <?php echo ($accentcolor); ?>;
	}
	


</style>



<!-- *************************************************************************
*****************              CUSTOM CSS              ********************
************************************************************************** -->
<style type="text/css">
<?php 
if ( function_exists( 'get_option_tree') ) {
  $css = get_option_tree( 'vn_custom_css' );
}
?>
    <?php echo ($css); ?>
</style>

</head>

<?php $theme_options = get_option('option_tree'); ?>

<body <?php body_class(); ?>>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$topmessagearea = get_option_tree( 'vn_topmessagearea' );
} ?>

<?php if (isset($topmessagearea)) { ?>

<!-- Start of outer wrapper -->
<header id="outer_wrapper">

<!-- Start of top message area -->
<div id="top_message_area">
<?php echo stripslashes($topmessagearea); ?>

</div><!-- End of top message area -->

</header><!-- End of outer wrapper -->

<?php } ?>

<!-- Start of bdywrapper -->
<div class="bdywrapper">

<!-- Start of nav wrapper -->
<div id="nav_wrapper">

<!-- Start of top logo -->
<div id="top_logo">
<a href="<?php bloginfo('siteurl'); ?>"><?php 
if ( function_exists( 'get_option_tree' ) ) {
$logopath = get_option_tree( 'vn_logo' );
} ?><img src="<?php echo $logopath; ?>" alt="logo" /></a>

</div><!-- End of top logo -->

<!-- Start of searchbox -->
<div id="searchbox">
    
    <a id="facebook-share" target="_blank" href="http://www.facebook.com/share.php?u=http://lovecouragehope.bastionserve.net/" title="share on facebook">Share on Facebook</a>
    
<?php get_search_form(); ?>

</div><!-- End of searchbox -->

<!-- Start of clear fix --><div class="clear"></div>

</div><!-- End of nav wrapper --> 

<!-- Start of topmenu wrapper -->
<div id="topmenu_wrapper">

<!-- Start of topmenu -->
<nav id="topmenu">  
<?php wp_nav_menu(array('menu_class'=>'sf-menu')); ?>

<!-- Start of clear fix --><div class="clear"></div>

</nav><!-- End of topmenu -->

</div><!-- End of topmenu wrapper -->

</div><!-- End of bdywrapper -->

<!-- Start of midsectionhr --><div id="midsectionhr"></div>

<!-- Start of bdywrapper -->
<div class="bdywrapper">